#!/bin/sh

#  Copy-Script.sh
#  Pods
#
#  Created by Sasikumar on 07/01/20.
#  
mkdir -p "${CONFIGURATION_BUILD_DIR}/${FRAMEWORKS_FOLDER_PATH}/Widevine/arm64-iphoneos"

# Copy arm64-iphoneos Frameworks
cp -a "${PODS_ROOT}/MOBIPlayer/Widevine/arm64-iphoneos/widevine_cdm_secured_ios_tmux.framework" "${CONFIGURATION_BUILD_DIR}/${FRAMEWORKS_FOLDER_PATH}/Widevine/arm64-iphoneos/widevine_cdm_secured_ios_tmux.framework"
codesign --verbose --force --sign "$EXPANDED_CODE_SIGN_IDENTITY" "${CONFIGURATION_BUILD_DIR}/${FRAMEWORKS_FOLDER_PATH}/Widevine/arm64-iphoneos/widevine_cdm_secured_ios_tmux.framework/widevine_cdm_secured_ios_tmux"

# Copy armv7-iphoneos Frameworks
mkdir -p "${CONFIGURATION_BUILD_DIR}/${FRAMEWORKS_FOLDER_PATH}/Widevine/armv7-iphoneos"
cp -a "${PODS_ROOT}/MOBIPlayer/Widevine/armv7-iphoneos/widevine_cdm_secured_ios_tmux.framework" "${CONFIGURATION_BUILD_DIR}/${FRAMEWORKS_FOLDER_PATH}/Widevine/armv7-iphoneos/widevine_cdm_secured_ios_tmux.framework"
codesign --verbose --force --sign "$EXPANDED_CODE_SIGN_IDENTITY" "${CONFIGURATION_BUILD_DIR}/${FRAMEWORKS_FOLDER_PATH}/Widevine/armv7-iphoneos/widevine_cdm_secured_ios_tmux.framework/widevine_cdm_secured_ios_tmux"

# Copy armv7s-iphoneos Frameworks
mkdir -p "${CONFIGURATION_BUILD_DIR}/${FRAMEWORKS_FOLDER_PATH}/Widevine/armv7s-iphoneos"
cp -a "${PODS_ROOT}/MOBIPlayer/Widevine/armv7s-iphoneos/widevine_cdm_secured_ios_tmux.framework" "${CONFIGURATION_BUILD_DIR}/${FRAMEWORKS_FOLDER_PATH}/Widevine/armv7s-iphoneos/widevine_cdm_secured_ios_tmux.framework"
codesign --verbose --force --sign "$EXPANDED_CODE_SIGN_IDENTITY" "${CONFIGURATION_BUILD_DIR}/${FRAMEWORKS_FOLDER_PATH}/Widevine/armv7s-iphoneos/widevine_cdm_secured_ios_tmux.framework/widevine_cdm_secured_ios_tmux"

# Copy x86_64-iphonesimulator Frameworks
mkdir -p "${CONFIGURATION_BUILD_DIR}/${FRAMEWORKS_FOLDER_PATH}/Widevine/x86_64-iphonesimulator"
cp -a "${PODS_ROOT}/MOBIPlayer/Widevine/x86_64-iphonesimulator/widevine_cdm_secured_ios_tmux.framework" "${CONFIGURATION_BUILD_DIR}/${FRAMEWORKS_FOLDER_PATH}/Widevine/x86_64-iphonesimulator/widevine_cdm_secured_ios_tmux.framework"

# Copy i386-iphonesimulator Frameworks
mkdir -p "${CONFIGURATION_BUILD_DIR}/${FRAMEWORKS_FOLDER_PATH}/Widevine/i386-iphonesimulator"
cp -a "${PODS_ROOT}/MOBIPlayer/Widevine/i386-iphonesimulator/widevine_cdm_secured_ios_tmux.framework" "${CONFIGURATION_BUILD_DIR}/${FRAMEWORKS_FOLDER_PATH}/Widevine/i386-iphonesimulator/widevine_cdm_secured_ios_tmux.framework"
 

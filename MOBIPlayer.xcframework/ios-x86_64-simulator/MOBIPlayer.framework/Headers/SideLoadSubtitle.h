//
//  SideLoadSubtitle.h
//  MOBIPlayer
//
//  Created by Sasikumar on 27/06/19.
//  Copyright © 2019 Sasikumar. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SideLoadSubtitle : NSObject
// URL of the physical media file.
@property NSURL *sourceURL;
// Language of the stream .
@property NSString *languageCode;
// Language of the stream .
@property NSString *displayName;
// Downloaded subtitle text .
@property NSString *text;
-(id)initWithSourceURL:(NSURL *)sourceURL
       andLanguageCode:(NSString *)languageCode
        andDisplayName:(NSString *)displayName
        andSubtitleText:(nullable NSString *)text;
@end

NS_ASSUME_NONNULL_END

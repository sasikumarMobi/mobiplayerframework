// Copyright 2017 Google Inc. All rights reserved.

#import <Foundation/Foundation.h>

// Locate the url for a file with a given filename.
NSURL *CDMDocumentFileURLForFilename(NSString *filename, NSString *subdirectory);

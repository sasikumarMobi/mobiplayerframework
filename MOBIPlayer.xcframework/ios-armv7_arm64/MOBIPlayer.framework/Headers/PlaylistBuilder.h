// Copyright 2018 Google Inc. All rights reserved.

#import <Foundation/Foundation.h>

@class MediaStream;

// Protocol to pull player time, used in playlist building.
@protocol PlaylistBuilderTimeProvider
- (float)getCurrentTime;
@end

extern NSString *kLocalPlaylistReadyNotification;

// Creates a new HLS playlist based on an MPEG-DASH manifest.
@interface PlaylistBuilder : NSObject

@property(nonatomic, weak) id<PlaylistBuilderTimeProvider> timeProvider;
// URL of the incoming DASH Manifest (MPD) file.
@property(nonatomic, strong) NSURL *MPDURL;
// Array containing all the child streams within the DASH Manifest (MPD).
@property NSMutableArray<MediaStream *> *streams;
// Array containing all the child streams within the DASH Manifest (MPD).
@property NSMutableArray<MediaStream *> *streamsWithSubtitles;
// Master HLS Playlist that is created to contain high level info about the child streams
// (bandwidth, codec, URL of stream, etc.)
@property NSString *variantPlaylist;

// Creates the Master/Variant Playlist
- (NSString *)variantPlaylistForStreams:(NSArray<MediaStream *> *)parsedMPD;
// Creates an HLS playlist that lists all of the TS segments within the stream.
// Requires an input stream to be used.
- (NSString *)childPlaylistForStream:(MediaStream *)stream;
// Obtains the actual data for the given stream.
- (void)loadStream:(MediaStream *)stream downloadData:(BOOL)downloadData;
// XML Parsing of the DASH Manifest that populates the Stream object values.
- (void)processMPD:(NSURL *)MPDURL
      downloadData:(BOOL)downloadData
    withCompletion:(void (^)(NSError *))completion;
// XML Parsing of the DASH Manifest that updates the Stream object values.
- (void)updateStream:(MediaStream *)stream
 WithCompletionBlock:(void (^)(MediaStream *stream, NSError *error))completion;
// Creates TS segments based on downloading a specific byte range.
- (NSData *)tsDataForIndex:(int)index segment:(int)segment;
// Verifies all Streams have been processed and initiates a Notification to begin playack of the
// transmuxed HLS content.
- (void)streamReady:(MediaStream *)stream;
// Destroys the playlist builder. Note that this is a synchronous call and it may block
// briefly.
- (void) destroy;
@end

//
//  MOBIView+IBInspectable.swift
//  MOBIPlayer
//
//  Created by Sasikumar on 01/01/20.
//  Copyright © 2020 Sasikumar. All rights reserved.
//

import UIKit

enum AssociatedKeys {
    static var supportOrientation = "supportOrientation"
}
enum ControlOrientation: Int {
    case both
    case portrait
    case landscape
}
public extension UIView {
    @IBInspectable
    var supportOrientation: Int {
        get { return currentSupportOrientation.rawValue }
        set { currentSupportOrientation = ControlOrientation(rawValue: newValue) ?? .both }
    }
    internal var currentSupportOrientation: ControlOrientation! {
        get { return objc_getAssociatedObject(self, &AssociatedKeys.supportOrientation) as? ControlOrientation ?? ControlOrientation.both }
        set { objc_setAssociatedObject(self, &AssociatedKeys.supportOrientation, newValue, objc_AssociationPolicy(rawValue: 769)!) }
    }
}

//
//  PSSH.h
//  Custom Video Player
//
//  Created by Sasikumar on 23/03/18.
//  Copyright © 2018 Sasikumar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CdmWrapper.h"
typedef void (^PSSHHelperCompletionBlock)(NSURL *, NSTimeInterval);
@interface PSSHHelper : NSObject <ExpireTimeDelegate> {
    NSURL * fileUrl;
}
- (BOOL)findPSSH:(NSData *)initializationData fileURL:(NSURL*)fileURL andreleaseLicense:(BOOL)releaseLicense andGetLicenseInfo:(BOOL)getLicenseInfo;
@property(nonatomic) BOOL getLicenseInfo;
@property BOOL releaseLicense;
@property  PSSHHelperCompletionBlock returnBlock;
@end

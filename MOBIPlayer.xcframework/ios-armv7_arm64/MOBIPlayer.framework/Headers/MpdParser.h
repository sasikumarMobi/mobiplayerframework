// Copyright 2018 Google Inc. All rights reserved.

#import "MediaStream.h"

@interface MPDParser : NSObject <NSXMLParserDelegate>

// Array of streams found in the XML manifest.
@property(nonatomic, strong) NSMutableArray<MediaStream *> *streams;

- (instancetype)initWithMPDData:(NSData *)MPDData;

// Remove media files listed in Manifest.
+ (void)deleteFilesInMPD:(NSURL *)MPDURL;
// Being parsing of Manifest by passing URL of streaming content.
+ (NSMutableArray<MediaStream *> *)streamArrayFromMPD:(PlaylistBuilder *)playlistBuilder
                                              MPDData:(NSData *)MPDData
                                              baseURL:(NSURL *)baseURL
                                         storeOffline:(BOOL)storeOffline
                                           isSubtitle:(BOOL)isSubtitle;
// Convert MPEG Dash formatted duration into seconds.
- (NSUInteger)convertDurationToSeconds:(NSString *)string;
@end
